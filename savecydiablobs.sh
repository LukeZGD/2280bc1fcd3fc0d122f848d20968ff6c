#!/bin/bash

device=$1
ecid=$2

if [[ -z $device || -z $ecid ]]; then
    echo "* Save Cydia blobs for your device"
    echo "Usage:"
    echo "    $0 [ProductType] [ECID]"
    echo
    echo "Example:"
    echo "    $0 iPad2,1 123456789012"
    echo
    exit 1
fi

echo "* Saving Cydia blobs for $device"
echo "* ECID: $ecid"

json=$(curl "https://firmware-keys.ipsw.me/device/$device")
len=$(echo "$json" | jq length)
builds=()
i=0
while (( i < len )); do
    builds+=($(echo "$json" | jq -r ".[$i].buildid"))
    ((i++))
done

mkdir $device 2>/dev/null
cd $device
for build in ${builds[@]}; do
    printf "\n$build "
    # https://github.com/TheRealClarity/tsschecker
    ../tsschecker -d $device -e $ecid -c -s -g 0x1111111111111111 --buildid $build >/dev/null
    [[ $(ls *$build* 2>/dev/null) ]] && printf "saved" || printf "failed"
done
echo
cd ..
